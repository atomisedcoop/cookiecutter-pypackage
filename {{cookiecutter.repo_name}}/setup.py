#!/usr/bin/env python
# -*- coding: utf-8 -*-
import os
import sys

from setuptools import setup


if sys.argv[-1] == 'publish':
    os.system('python setup.py sdist upload')
    sys.exit()

readme = open('README.rst').read()
history = open('HISTORY.rst').read().replace('.. :changelog:', '')

install_requires = []

def find_packages(path, src):
    packages = []
    for pkg in [src]:
        for _dir, subdirectories, files in (
                os.walk(os.path.join(path, pkg))):
            if '__init__.py' in files:
                tokens = _dir.split(os.sep)[len(path.split(os.sep)):]
                packages.append(".".join(tokens))
    return packages

setup(
    name='{{ cookiecutter.repo_name }}',
    version='{{ cookiecutter.version }}',
    description='{{ cookiecutter.project_short_description }}',
    long_description=readme + '\n\n' + history,
    author='{{ cookiecutter.full_name }}',
    author_email='{{ cookiecutter.email }}',
    url='https://bitbucket.org/{{ cookiecutter.bitbucket_username }}/{{ cookiecutter.repo_name }}',
    packages=find_packages('.','{{ cookiecutter.src_name }}')
    include_package_data=True,
    install_requires=install_requires,
    license="BSD",
    zip_safe=False,
    keywords='{{ cookiecutter.src_name }}',
    classifiers=[
        'Development Status :: 2 - Pre-Alpha',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: BSD License',
        'Natural Language :: English',
        "Programming Language :: Python :: 2",
        'Programming Language :: Python :: 2.6',
        'Programming Language :: Python :: 2.7',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.3',
    ],
    test_suite='tests',
)
